/*
 * Este programa visualiza el máximo
 * común divisor de dos números
 */

package programacionModular;

import java.util.Scanner;

public class Mcd {

	public static void main(String[] args) {
		int numero1 = pedirNumero1();
		int numero2 = pedirNumero2();
		int mcd = calcularMcd(numero1, numero2);
		verResultado(numero1, numero2, mcd);
	}

	public static int pedirNumero1() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce un número");
		return entrada.nextInt();
	}
	public static int pedirNumero2() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce otro número");
		return entrada.nextInt();
	}
	public static int calcularMcd(int numero1, int numero2) {
		while(numero1 != numero2) {
			if (numero1 > numero2) {
				numero1 = numero1- numero2;
			}else {
				numero2 = numero2-numero1;
			}
		}
		return numero1;
	}
	public static void verResultado(int numero1, int numero2, int mcd) {
		System.out.println("El mCd de " + numero1 + " y " + numero2 + " es " + mcd);
	}
}
