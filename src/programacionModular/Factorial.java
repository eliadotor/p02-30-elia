package programacionModular;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		
		int numero = pedirNumero();
		int factorial = calcularFactorial(numero);
		verResultado(numero, factorial);
		
		/*para calcular el factorial desde 1 hasta 5, 
		llamamos a la funcion y le pasamos como parametro el contador*/
		for(int i=1; i<=5; i++){
			System.out.println(calcularFactorial(i));
		}
	}
		public static int pedirNumero() {
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduzca un número para calcular su factorial");
			return entrada.nextInt();
		}
		
		public static int calcularFactorial(int numero) {
			int factorial = 1;
			for(int i=numero; i > 0; i--) {
				factorial = factorial * i;
			}
			return factorial;
			
		}
		
		public static void verResultado(int numero, int factorial) {
			System.out.println(numero + "! = " + factorial);

		}
		
		
}	
		
		
		
		
	


