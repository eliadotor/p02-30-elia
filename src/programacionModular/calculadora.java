package programacionModular;

import java.util.Scanner;

public class calculadora {
	
	static Scanner entrada = new Scanner(System.in);

	public static void main(String[] args) {
		boolean salir = false;
		do {
			int opcion = verMenu();
			salir = (opcion == 5);
			if(!salir) {
				elegirOperacion(opcion);
			}else {
				System.out.println("Fin del programa");
			}
		}while(!salir);
	}
	
	public static int verMenu() {
		int opcion;
		boolean opcionValida;
		System.out.println("Eliga una opcion: \n 1.Suma \n 2.Resta \n 3.Multiplicación \n 4.División \n 5. Salir del programa");
		do {
			opcion= entrada.nextInt();
			opcionValida = (opcion > 0 && opcion <= 5);
			
			if(!opcionValida){
				System.out.println("Elija una opción del 1 al 4");
			}
		
		}while(!opcionValida);
		return opcion;
	}
	
	public static void elegirOperacion(int opcion) {
		boolean opcionValida;
		
		System.out.println("Introduzca un numero");
		double num1 = entrada.nextDouble();
		System.out.println("Introduzca otro numero");
		double num2 = entrada.nextDouble();
		while(opcion == 4 && num2 ==0) {
			System.out.println("El segundo número no puede ser 0.");
			num2 = entrada.nextDouble();
		}
		double resultado = 0;
		switch (opcion) {
			case 1: resultado = hacerSuma(num1, num2);
			break;
			case 2: resultado = hacerResta(num1, num2);
			break;
			case 3: resultado = hacerMultiplicacion(num1, num2);
			break;
			case 4: resultado = hacerDivision(num1, num2);
			break;
		}
		verResultado(resultado);
		
	}
	
	public static double hacerSuma(double num1, double num2) {
		return num1 + num2;
	}
	
	public static double hacerResta(double num1, double num2) {
		return num1 - num2;
	}

	public static double hacerMultiplicacion(double num1, double num2) {
		return num1 * num2;
	}

	public static double hacerDivision(double num1, double num2) {
		return num1 / num2;
	}
	
	public static void verResultado(double resultado) {
		System.out.println(resultado);
		System.out.println();

	}
	
	

}  
