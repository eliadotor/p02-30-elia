package programacionModular;

import java.util.Scanner;

public class CambioBase {
	
	static Scanner entrada = new Scanner(System.in);

	public static void main(String[] args) {
		int numero = pedirNumero();
		int base = pedirBase();
		conversorBase(numero,base);
		
	}
		

	public static int pedirNumero() {
		System.out.println("Introduce un numero");
		boolean numeroValido;
		int numero;
		do {
			numero = entrada.nextInt();
			numeroValido = numero > 0;
			if(!numeroValido) {
				System.out.println("ERROR. Introduce un número mayor que 0");
			}
		}while(!numeroValido);
		return numero;	
	}
	
	public static int pedirBase() {
		System.out.println("Introduce la base");
		boolean baseValida;
		int base;
		do {
			base = entrada.nextInt();
			baseValida = (base >1) && (base<10);
			if(!baseValida) {
				System.out.println("ERROR. Introduce un número mayor que 1 y menor que 10");
			}
		}while(!baseValida);
		return base;	
	}
	
	public static void conversorBase(int numero, int base) {
		int resto = numero % base;
		if(numero<base) {
			System.out.print(numero);
		}else {
			conversorBase((numero/base), base);
			System.out.print(resto);
		}
		
	}
	
	
}
