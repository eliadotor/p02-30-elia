/*Elia Dotor Puente
 * El programa funciona.
 */
package examen;

import java.io.IOException;
import java.util.Scanner;

public class Figura {
	public static void main(String[] args) throws IOException {
		
		char caracter = 'P';
		int altura = 5;
		for(int linea = 1; linea<=altura; linea++) {
			if(linea<=3) {
				for (int i = 1; i<=linea; i++) {
					System.out.print(caracter);
				}
			}else {
				for (int i = altura; i>=linea; i--) {
					
					System.out.print(caracter);
				}
			}
			System.out.println();
		}
	}
}
