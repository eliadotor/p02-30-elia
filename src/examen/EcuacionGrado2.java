/*Elia Dotor Puente
 * El programa funciona.
 */
package examen;

import java.util.Scanner;

public class EcuacionGrado2 {

	public static void main(String[] args) {
		System.out.println("Introduce coeficiente a");
		double a = pedirCoeficiente();
		System.out.println("Introduce coeficiente b");
		double b = pedirCoeficiente();
		System.out.println("Introduce coeficiente c");
		double c = pedirCoeficiente();
		comprobarGrado(a, b, c);

	}
	
	public static double pedirCoeficiente() {
		Scanner entrada = new Scanner(System.in);
		return entrada.nextInt();
	}
	
	public static void comprobarGrado(double a, double b, double c) {
		if(a==0 && b==0) {
			System.out.println("No es una ecuación");
		}else if(a==0 && b!=0) {
			System.out.println("Es una ecuación de grado 1. La solución es x= " + ((-c)/b));
		}else {
			double discriminante = b*b-4*a*c;
			if(discriminante>=0) {
				double re= -b/(2*a);
				double im = (Math.sqrt(Math.abs(discriminante))/(2*a));
				System.out.println("Las raíces reales son " + (re+im) + "y" + (re-im));
				
				
			}else{
				System.out.println("No existe solución dentro del campo de los números reales");
			}
		}
	}

}
