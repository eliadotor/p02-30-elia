/*Elia Dotor Puente
 * El programa funciona.
 */

package examen;

import java.util.Scanner;

public class Aleatorio {

	public static void main(String[] args) {
		int numSecreto = (int) (Math.random()*100+1);
		boolean numeroValido = false;
		boolean salir = false;
		boolean numeroCorrecto;
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduce un número entre 1 y 100. Para finalizar el programa introduzca -1");
		int numero;
		do {
			do {
				numero = entrada.nextInt();
				numeroValido = (numero >= 1 && numero <= 100 || numero == -1);
				if(!numeroValido){
					System.err.println("ERROR. Introduce un número comprendido entre 1 y 100");
				}
			}while(!numeroValido);
			salir = numero == -1;
			if (!salir) {
				numeroCorrecto = (numero == numSecreto);
				if(!numeroCorrecto) {
					if(numero>numSecreto) {
						System.out.println("El número aleatorio es menor");
					}else {
						System.out.println("El número aleatorio es mayor");
					}
				}else {
					System.out.println("Enhorabuena! El número aleatorio es " + numero + ".");
				}
				
			}else {
				System.out.println("Te has rendido.");
			}
			
		}while(!salir);
		
	}

}
