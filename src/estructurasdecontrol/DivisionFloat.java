package estructurasdecontrol;

import java.util.Scanner;

public class DivisionFloat {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		//Introducimos un numero
		
		System.out.println("Introduce un número real");
		double dividendo = entrada.nextDouble();
		
		//Introducimos un segundo número
		System.out.println("Introduce un segundo número real");
		double divisor = entrada.nextDouble();
		
		
		if(divisor != 0) {
			System.out.println("El resultado es " + (dividendo/divisor));
			
		}else {
			System.out.println("Error");
			}
	}

}
