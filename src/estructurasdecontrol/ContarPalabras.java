package estructurasdecontrol;

import java.io.IOException;

public class ContarPalabras {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Introduce una frase y terminala en .");
		
		int contadorEspacios = 0;
		boolean salir = false;
		//Introducimos el caracter de la frase
		char frase = (char)System.in.read();
		
		if(frase == '.') {
			System.out.println("La frase no tiene ninguna palabra.");
		}else{
			
			do {
				frase = (char)System.in.read();
				//comprobamos si ha tecleado un . con una expresión lógica
				salir = (frase == '.');
				if(!salir) {
					//comprobamos cuantos espacios hay añadiendo uno al contador cada vez que encuentre uno
					if(frase == ' ') {
						contadorEspacios++;
					}
				}
			}while(!salir);
		
		
		System.out.println("La frase tiene " + (contadorEspacios + 1) + " palabras");	//El número de palabras será el número de espacios + 1

		}
	}
	
}


/*Elia cambios*/
