package estructurasdecontrol;


import java.io.IOException;

public class ContarPalabrasWhile {

	public static void main(String[] args) throws IOException {
		int palabras = 0;
		//Introducimos el caracter de la frase
		char caracter = (char) System.in.read();
		boolean finPalabra = true;
		while (caracter != '.'){
			if(finPalabra == true && caracter != ' '){
				palabras++;
				finPalabra = false;
			}else if(caracter == ' ') {
				finPalabra = true;
			}
					
			caracter = (char) System.in.read();
		}
		
		System.out.println("La frase tiene " + palabras + " palabras");	//El número de palabras será el número de espacios + 1
	}
}
