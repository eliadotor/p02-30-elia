package estructurasdecontrol;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca un número para calcular su factorial");
		int num = entrada.nextInt();
		int factorial = 1;
		
		for(int i=num; i > 0; i--) {
			factorial = factorial * i;
		}
		
		System.out.println(num + "! = " + factorial);
		
	}

}
