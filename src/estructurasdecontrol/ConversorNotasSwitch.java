/*convierte una nota alfabetica a una numerica
 utilizando la estructura switch
 */

package estructurasdecontrol;


import java.io.IOException;
public class ConversorNotasSwitch {

	public static void main(String[] args) throws IOException{
		//Introducimos por teclado la nota alfabetica
		
		//Scanner entrada = new Scanner(System.in);
	
		System.out.println("Introduce tu nota");
		//char notaAlfabetica = entrada.next.charAt(0);
		
		char notaAlfab = (char) System.in.read();
		
		//declaramos una variable con la nota numerica
		int notaNum = 0;
		//variable booleana para comprobar si la nota es correcta
		boolean notaCorrecta = true;
		
		//convertimos la nota alfabetica a numerica
		
		switch (notaAlfab) {
			case 'I' : notaNum = 4; 
				break;
			case 'F' : notaNum = 5;
				break;
			case 'B' : notaNum = 6;
				break;
			case 'N' : notaNum = 7;
				break;
			case 'S' : notaNum = 9;
				break;
			default:
			        System.out.println("La nota introducida no es correcta");
			        notaCorrecta = false;
			        break;
			}
		
		//Comprobamos si la nota es correEnviamos a pantalla la nota numerica
		if (notaCorrecta) {
		System.out.println("Tu nota es " + notaNum);
		}
		
	}

}
