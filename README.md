# Ejecuto comando Linux "ls" para listar el contenido de la carpeta del Repositorio de Elia y llegar al archivo a modificar

~~~
➜  p02-20-elia git:(master) ✗ ls
README.md  src

➜  p02-20-elia git:(master) ✗ cd src
➜  src git:(master) ✗ ls
clases  herenciapolimorfismo  interfaces  proyectoCuentas
➜  src git:(master) ✗ cd clases
➜  clases git:(master) ✗ ls
Circulo.java         Racional.java            TestRacional.java
ClaseCharacter.java  Rectangulo.java          TestRectangulo.java
ClaseString.java     TestCirculo.java
CuentaBancaria.java  TestCuentaBancaria.java
~~~

# Ejecuto comando "git init" para crear Repositorio Local 
~~~
➜  clases git:(master) ✗ git init
Inicializado repositorio Git vacío en /home/jsfs/code/p02-20-elia/src/clases/.git/
~~~

# Ejecuto comando "git init" para crear Repositorio Local 

~~~

➜  clases git:(master) ✗ git status
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	Circulo.java
	ClaseCharacter.java
	ClaseString.java
	CuentaBancaria.java
	Racional.java
	Rectangulo.java
	TestCirculo.java
	TestCuentaBancaria.java
	TestRacional.java
	TestRectangulo.java

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)
~~~

# Ejecuto comando "git add" para añadir el archivo al Repositorio Local

~~~

➜  clases git:(master) ✗ git add ClaseString.java
~~~

# Ejecuto comando "git status" para ver el estado (Unstaged, Staged,...) del archivo a modificar 'ClasesString.java'

~~~
➜  clases git:(master) ✗ git status
En la rama master

No hay commits todavía

Cambios a ser confirmados:
  (usa "git rm --cached <archivo>..." para sacar del área de stage)

	nuevo archivo:  ClaseString.java

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	Circulo.java
	ClaseCharacter.java
	CuentaBancaria.java
	Racional.java
	Rectangulo.java
	TestCirculo.java
	TestCuentaBancaria.java
	TestRacional.java
	TestRectangulo.java

~~~


# Ejecuto comando "git commit -m " para añadir el comentario de los cambios realizados el archivo

~~~

➜  clases git:(master) ✗ git commit -m "Añado mi nombre al final del documento"
[master (commit-raíz) ffd2ec3] Añado mi nombre al final del documento
 1 file changed, 69 insertions(+)
 create mode 100644 ClaseString.java
 
~~~


# Ejecuto comando "git remote add origin" para poner el Repositorio de Elia como 'origin'

~~~

➜  clases git:(master) ✗ git remote add origin https://bitbucket.org/eliadotor/p02-20-elia.git     
~~~


# Ejecuto comando "git push" para subir el archivo con los cambios al Repositorio Remoto de Elia

~~~

➜  clases git:(master) ✗ git push origin master
Username for 'https://bitbucket.org': jorgesf
Password for 'https://jorgesf@bitbucket.org': 
To https://bitbucket.org/eliadotor/p02-20-elia.git
 ! [rejected]        master -> master (fetch first)
error: fallo el push de algunas referencias a 'https://bitbucket.org/eliadotor/p02-20-elia.git'
ayuda: Actualizaciones fueron rechazadas porque el remoto contiene trabajo que
ayuda: no existe localmente. Esto es causado usualmente por otro repositorio 
ayuda: realizando push a la misma ref. Quizás quiera integrar primero los cambios
ayuda: remotos (ej. 'git pull ...') antes de volver a hacer push.
ayuda: Vea 'Notes about fast-forwards0 en 'git push --help' para detalles.
~~~

# Ejecuto comando "git push" para subir el archivo con los cambios al Repositorio Remoto de Elia
~~~
Despues de ejecutar el comando anterior (git oush origin master" aparece una ventana emergente de MERGE, añado el comentario y todo actualizado
~~~
